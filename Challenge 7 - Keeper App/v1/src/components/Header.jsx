import React from "react";

function Header() {
  return (
    <header className="heading">
      <h1>Keeper App</h1>
    </header>
  );
}

export default Header;
