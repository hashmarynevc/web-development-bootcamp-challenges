import React, { useState }  from "react";
import DeleteIcon from '@material-ui/icons/Delete';

function Note(props) {
  
  function handleClick() {
    props.onDelete(props.id);
  }

  const [isEditable, setEditable] = useState(false);

  function editNote(){
    setEditable(prevValue=>{
      return !prevValue;
    })

  }


  return (
    <div className="note">
    <button onClick={editNote} >EDIT</button>
      <h1>{props.title}</h1>
      <p  contentEditable={isEditable ? true : false} >{props.content} </p>
      <button onClick={handleClick}>
      <DeleteIcon/>
      </button>
    </div>
  );
}

export default Note;
