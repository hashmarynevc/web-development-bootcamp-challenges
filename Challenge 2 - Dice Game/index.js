document.querySelector(".roll").addEventListener("click",function(){
    var player1 = Math.floor((Math.random() * 6) + 1);
    var player2 = Math.floor((Math.random() * 6) + 1);
    document.querySelector(".img1").src = "images/dice" + player1 + ".png";
    document.querySelector(".img2").src = "images/dice" + player2 + ".png";
    winGame(player1,player2);
    
});

var player1Points=0;
var player2Points=0;

function winGame(num1, num2){
    if(num1>num2)
    {
        document.querySelector(".msg").innerHTML = "🚩Player 1 wins"; 
        player1Points++;
        console.log("score1 " + player1Points);
        document.querySelector(".score1").innerHTML = player1Points;
    }
    else if(num2>num1){
        document.querySelector(".msg").innerHTML = "Player 2 wins 🚩"; 
        player2Points++;
        console.log(document.querySelector(".score2").innerHTML);
        document.querySelector(".score2").innerHTML = player2Points;
    }
    else{
        document.querySelector(".msg").innerHTML = "Draw!"; 
    }
}
