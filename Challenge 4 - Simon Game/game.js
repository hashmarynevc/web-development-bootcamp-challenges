var gamePattern = [];
var userClickedPattern = [];
var level = 0;
var start = false;
var btnColors = ["red", "blue", "green", "yellow"];
$(document).keypress(function () {
    if (!start) {
        $("#level-title").text("Level " + level);
        nextSequence();
        start = true;
    }
});

//user pattern
$(".btn").click(function () {
    var userChosenColour = $(this).attr("id");
    userClickedPattern.push(userChosenColour);
    $("#" + userChosenColour).fadeOut(100).fadeIn(100);
    playSound(userChosenColour);
    checkAnswer(userClickedPattern.length - 1);
    //console.log(userClickedPattern);
});



//game pattern
function nextSequence() {
    level++;
    $("#level-title").text("Level " + level);
    var randomNumber = Math.floor(Math.random() * 4);
    var randomChosenColour = btnColors[randomNumber];
    gamePattern.push(randomChosenColour);
    //$("#someElement").fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
    $("#" + randomChosenColour).fadeOut(100).fadeIn(100);
    playSound(randomChosenColour);
    //console.log(gamePattern);
    //console.log(userClickedPattern);

}

//play sound
function playSound(key) {
    switch (key) {
        case "blue":
            var audio = new Audio("sounds/blue.mp3");
            audio.play();
            break;
        case "green":
            var audio = new Audio("sounds/green.mp3");
            audio.play();
            break;
        case "red":
            var audio = new Audio("sounds/red.mp3");
            audio.play();
            break;
        case "yellow":
            var audio = new Audio("sounds/yellow.mp3");
            audio.play();
            break;
        default:
            alert("error");
            break;
    }
}

//check answer
function checkAnswer(currentLevel) {
    if (gamePattern[currentLevel] === userClickedPattern[currentLevel]) {

        console.log("success");

        if (userClickedPattern.length === gamePattern.length) {

            setTimeout(function () {
                nextSequence();
            }, 1000);

        }

    } else {

        console.log("wrong");
        var audio = new Audio("sounds/wrong.mp3")
        audio.play();
        
        $("body").addClass("game-over");
        setTimeout(function () {
            $("body").removeClass("game-over");
        }, 200);
        $("#level-title").text("Game Over, Press Any Key to Restart");
        Swal.fire({
            title: 'Error!',
            text: 'Do you want to continue',
            icon: 'error',
            confirmButtonText: 'Cool'
          })
        startOver();
    }
}

//start over 
function startOver() {
    start = false;
    level = 0;
    gamePattern = null;
    userClickedPattern = null;
}